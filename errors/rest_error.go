package errors

import (
	"encoding/json"
)

type RestError struct {
	Status  int
	Message string
}

func NewRestError(status int, message string) *RestError {
	return &RestError{
		Status:  status,
		Message: message,
	}
}

func (e *RestError) Error() string {
	res, _ := json.Marshal(e)
	return string(res)
}
