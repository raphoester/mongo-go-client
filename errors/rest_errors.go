package errors

import "net/http"

func NewInternalServerError() *RestError {
	return NewRestError(http.StatusInternalServerError, "")
}
