package log

import "os"

type Ilogger interface {
	Fatal(text string, v ...any)
	Core(text string, v ...any)
	Error(text string, v ...any)
	Info(text string, v ...any)
	Debug(text string, v ...any)
}

type logger struct {
	Level int
}

func NewLogger(level int) *logger {
	return &logger{
		Level: level,
	}
}

func (l logger) Fatal(text string, v ...any) {
	l.baseLogStderr(l.Level, 1, text, v...)
	os.Exit(2)
}

func (l logger) Core(text string, v ...any) {
	l.baseLog(l.Level, 1, text, v...)
}

func (l logger) Error(text string, v ...any) {
	l.baseLog(l.Level, 2, text, v...)
}

func (l logger) Info(text string, v ...any) {
	l.baseLog(l.Level, 3, text, v...)
}

func (l logger) Debug(text string, v ...any) {
	l.baseLog(l.Level, 4, text, v...)
}
