package log

import (
	"fmt"
	"log"
	"os"
)

var std = log.New(os.Stderr, "", log.LstdFlags)

func (l logger) baseLog(logLevel int, emergency int, text string, v ...any) {
	if logLevel >= emergency {
		callerFunc := getSecondCallerFunctionName()
		kindMessage := getKindMessageStdout(emergency)
		format := formatText(kindMessage, callerFunc, text)
		log.Printf(format+"\n", v...)
	}
}

func (l logger) baseLogStderr(logLevel int, emergency int, text string, v ...any) {
	if logLevel >= emergency {
		callerFunc := getSecondCallerFunctionName()
		kindMessage := getKindMessageStderr(emergency)
		format := formatText(kindMessage, callerFunc, text)
		std.Output(2, fmt.Sprintf(format, v...))
	}
}
