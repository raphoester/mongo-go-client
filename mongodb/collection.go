package mongodb

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/raphoester/mongo-go-client/errors"
	"gitlab.com/raphoester/mongo-go-client/log"
	"go.mongodb.org/mongo-driver/mongo"
)

type Icollection interface {
	Name() string
	Aggregate(ctx context.Context, pipeline any) (ImanyResults, *errors.RestError)
	InsertOne(ctx context.Context, document any) (IinsertOneResult, *errors.RestError)
	Find(ctx context.Context, filter any, options ...FindOptions) (ImanyResults, *errors.RestError)
	UpdateOne(ctx context.Context, filter any, update any) (IupdateResult, *errors.RestError)
	DeleteOne(ctx context.Context, filter any) (IdeleteResult, *errors.RestError)
	UpdateMany(ctx context.Context, filter any, update any) (IupdateResult, *errors.RestError)
	FindOne(ctx context.Context, filter any) (IsingleResult, *errors.RestError)
	GetOne(ctx context.Context, filter any, dst any) *errors.RestError
	InsertMany(ctx context.Context, documents []any) (IinsertManyResult, *errors.RestError)
	DeleteMany(ctx context.Context, filter any) (IdeleteResult, *errors.RestError)
	// FindOneAndUpdate(ctx context.Context, filter any, update any) (*mongo.SingleResult, *errors.RestError)
	// FindOneAndDelete(ctx context.Context, filter any) (*mongo.SingleResult, *errors.RestError)
	// UpdateByID(ctx context.Context, filter any, update any) (IupdateResult, *errors.RestError)
}

type collection struct {
	c      *mongo.Collection
	logger log.Ilogger
}

func (c collection) Name() string {
	return c.c.Name()
}

func (c collection) Aggregate(ctx context.Context, pipeline any) (ImanyResults, *errors.RestError) {
	res, queryErr := c.c.Aggregate(ctx, pipeline)
	if queryErr != nil {
		c.logger.Error("failed executing aggregation pipeline %v | %s", pipeline, queryErr.Error())
		return nil, &errors.RestError{
			Status: http.StatusInternalServerError,
		}
	}

	c.logger.Debug("successfully executed aggregation pipeline %v", pipeline)
	return newManyResults(res, c.logger), nil
}

func (c collection) Find(ctx context.Context, filter any, options ...FindOptions) (results ImanyResults, err *errors.RestError) {
	if len(options) > 1 {
		c.logger.Error("only one option object is allowed")
		return nil, errors.NewInternalServerError()
	}

	opts := options[0].toOptions()

	res, queryErr := c.c.Find(ctx, filter, opts)
	if queryErr != nil {
		c.logger.Error("failed executing find operation | %s", queryErr.Error())
		return nil, &errors.RestError{
			Status: http.StatusInternalServerError,
		}
	}

	ret := newManyResults(res, c.logger)
	return ret, nil
}

func (c collection) UpdateOne(ctx context.Context, filter any, update any) (IupdateResult, *errors.RestError) {
	res, queryErr := c.c.UpdateOne(ctx, filter, update)
	if queryErr != nil {
		c.logger.Error("failed executing the following update operation : %v | %s", update, queryErr.Error())
		return nil, &errors.RestError{
			Status: http.StatusInternalServerError,
		}
	}

	if res.MatchedCount == 0 {
		c.logger.Info("no document in the %s collection matches the following filter : %v", c.Name(), filter)
		return nil, &errors.RestError{
			Message: "no document matches the filter",
			Status:  http.StatusNotFound,
		}
	}

	if res.ModifiedCount == 0 {
		c.logger.Info("no update needed for filter %v and update %v in %s collection", filter, update, c.Name())
		return nil, &errors.RestError{
			Message: "document is already in the desired state",
			Status:  http.StatusOK,
		}
	}

	c.logger.Debug("successfully updated document in %s collection", c.Name())
	return newUpdateResult(res), nil
}

func (c collection) UpdateMany(ctx context.Context, filter any, update any) (IupdateResult, *errors.RestError) {
	res, queryErr := c.c.UpdateMany(ctx, filter, update)
	if queryErr != nil {
		c.logger.Error("failed executing the following update operation : %v | %s", update, queryErr.Error())
		return nil, &errors.RestError{
			Status: http.StatusInternalServerError,
		}
	}

	if res.MatchedCount == 0 {
		c.logger.Info("no document in the %s collection matches the following filter : %v", c.Name(), filter)
		return nil, &errors.RestError{
			Message: "no document matches the filter",
			Status:  http.StatusNotFound,
		}
	}

	if res.ModifiedCount == 0 {
		c.logger.Info("no update needed for filter %v and update %v in %s collection", filter, update, c.Name())
		return nil, &errors.RestError{
			Message: "document is already in the desired state",
			Status:  http.StatusOK,
		}
	}

	c.logger.Debug("successfully updated document in %s collection", c.Name())
	return newUpdateResult(res), nil
}

func (c collection) FindOne(ctx context.Context, filter any) (IsingleResult, *errors.RestError) {
	res := c.c.FindOne(ctx, filter)
	if res.Err() == mongo.ErrNoDocuments {
		c.logger.Info("no document found in collection %s with filter %v", c.Name(), filter)
		return nil, &errors.RestError{
			Message: fmt.Sprintf("this %s does not exist", c.Name()),
			Status:  http.StatusNotFound,
		}
	} else if res.Err() != nil {
		c.logger.Error("failed getting document | %s", res.Err().Error())
		return nil, errors.NewInternalServerError()
	}

	c.logger.Debug("successfully retrieved document from filter %v in collection named %s", filter, c.Name())
	return newSingleResult(res, c.logger), nil
}

func (c collection) GetOne(ctx context.Context, filter any, dest any) *errors.RestError {
	res, err := c.FindOne(ctx, filter)
	if err != nil {
		c.logger.Info("failed getting document | %s", err.Error())
		return err
	}

	if err := res.Decode(dest); err != nil {
		c.logger.Error("failed parsing %s document to struct | %s", c.Name(), err.Error())
		return errors.NewInternalServerError()
	}

	return nil
}

func (c collection) DeleteOne(ctx context.Context, filter any) (IdeleteResult, *errors.RestError) {
	res, queryErr := c.c.DeleteOne(ctx, filter)
	if queryErr != nil {
		c.logger.Error("failed deleting document in collection %s with filter %v | %s", c.c.Name(), filter, queryErr.Error())
		return nil, &errors.RestError{
			Status: http.StatusInternalServerError,
		}
	}

	if res.DeletedCount == 0 {
		c.logger.Info("no document matches filter %v in collection %s", filter, c.Name())
		return nil, &errors.RestError{
			Status:  http.StatusNotFound,
			Message: "no document matches this filter",
		}
	}

	c.logger.Debug("successfully deleted document from collection named %v and filter filter %v", c.Name(), filter)
	return newDeleteResult(res), nil
}

func (c collection) InsertOne(ctx context.Context, document any) (IinsertOneResult, *errors.RestError) {
	res, queryErr := c.c.InsertOne(ctx, document)
	if queryErr != nil {
		c.logger.Error("failed inserting document %v in collection named %s | %s", document, c.Name(), queryErr.Error())
		return nil, &errors.RestError{
			Status: http.StatusInternalServerError,
		}
	}

	c.logger.Debug("successfully created document with id %v in collection named %s")
	return newInsertOneResult(res), nil
}

func (c collection) InsertMany(ctx context.Context, documents []any) (IinsertManyResult, *errors.RestError) {
	res, queryErr := c.c.InsertMany(ctx, documents)
	if queryErr != nil {
		c.logger.Error("failed inserting doucments in collection | %s")
		return nil, errors.NewInternalServerError()
	}

	return NewInsertManyResult(res), nil
}

func (c collection) DeleteMany(ctx context.Context, filter any) (IdeleteResult, *errors.RestError) {
	res, err := c.c.DeleteMany(ctx, filter)
	if err != nil {
		c.logger.Error("failed deleting documents from collection | %s", err.Error())
		return nil, errors.NewInternalServerError()
	}

	if res.DeletedCount == 0 {
		c.logger.Info("no documents have been deleted")
		return nil, errors.NewRestError(http.StatusOK, "no deletion needed")
	}

	return newDeleteResult(res), nil
}

// func (c collection) FindOneAndUpdate(ctx context.Context, filter any, update any) (*mongo.SingleResult, *errors.RestError) {
// 	return c.c.FindOneAndUpdate(ctx, filter, update)
// }

// func (c collection) FindOneAndDelete(ctx context.Context, filter any) (*mongo.SingleResult, *errors.RestError) {
// 	return c.c.FindOneAndDelete(ctx, filter)
// }

// func (c collection) UpdateByID(ctx context.Context, filter any, update any) (*mongo.UpdateResult, *errors.RestError) {
// 	return c.c.UpdateOne(ctx, filter, update)
// }
