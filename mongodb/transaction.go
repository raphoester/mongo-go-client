package mongodb

import (
	"context"
	"net/http"

	"gitlab.com/raphoester/mongo-go-client/errors"
	"gitlab.com/raphoester/mongo-go-client/internal/error_utils"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
)

// The callback func needs to return a custom error type that contains
// two fields : Status (int) and Message (string).
func (m mongodb) Transaction(callback func(Context) (any, error)) (any, *errors.RestError) {
	wc := writeconcern.New(writeconcern.WMajority())
	rc := readconcern.Snapshot()

	txnOpts := options.Transaction().SetWriteConcern(wc).SetReadConcern(rc)

	session, err := m.DB.Client().StartSession()
	if err != nil {
		m.logger.Error("failed creating session for transaction | %s", err.Error())
		return nil, &errors.RestError{
			Status: http.StatusInternalServerError,
		}
	}

	realCallback := func(ctx mongo.SessionContext) (any, error) {
		return callback(Context{ctx})
	}

	res, err := session.WithTransaction(context.TODO(), realCallback, txnOpts)
	if err != nil {
		restError := error_utils.NativeErrorToCustom(err)
		m.logger.Info("failed executing transaction | %s", restError.Error())
		return nil, restError
	}

	return res, nil
}

type Context struct {
	mongo.SessionContext
}
