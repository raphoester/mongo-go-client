package mongodb

import (
	"go.mongodb.org/mongo-driver/mongo/options"
)

type FindOptions struct {
	Fields    []string
	Paginator Paginator
}

type Paginator struct {
	PageNumber     int
	ResultsPerPage int
}

func (o FindOptions) toOptions() *options.FindOptions {
	ret := options.Find()
	if len(o.Fields) > 0 {
		ret.SetProjection(A{M{"name": 1}, M{"_id": 1}})
	}

	return ret
}
