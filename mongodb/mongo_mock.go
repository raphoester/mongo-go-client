package mongodb

import (
	"gitlab.com/raphoester/mongo-go-client/errors"
	"gitlab.com/raphoester/mongo-go-client/log"
)

type mongodbMock struct {
	logger log.Ilogger
}

func NewMongoMock(logger log.Ilogger) *mongodbMock {
	//lint:ignore S1021 _
	var ret Imongodb
	ret = &mongodbMock{
		logger: logger,
	}
	return ret.(*mongodbMock)
}

func (m *mongodbMock) Connect() error {
	return nil
}

func (m *mongodbMock) ConnectToCollection(name string) Icollection {
	//lint:ignore S1021 _
	var ret Icollection
	ret = NewCollectionMock(m.logger, name)
	return ret.(*collectionMock)
}

func (m *mongodbMock) Transaction(callback func(Context) (any, error)) (any, *errors.RestError) {
	// res, err := callback(context.TODO())
	return nil, nil
}
