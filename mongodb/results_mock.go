package mongodb

import (
	"gitlab.com/raphoester/mongo-go-client/errors"
	"gitlab.com/raphoester/mongo-go-client/log"
)

type manyResultsMock struct {
	Mocks  []ManyResultsMockContent
	logger log.Ilogger
}

type ManyResultsMockContent struct {
	Content any
	Error   *errors.RestError
}

func NewManyResultsMock() *manyResultsMock {
	return &manyResultsMock{}
}

func (r *manyResultsMock) All(dest *any) *errors.RestError {
	if len(r.Mocks) < 1 {
		r.logger.Error("no mocks data provided")
	}

	*dest = r.Mocks[0].Content
	err := r.Mocks[0].Error

	return err
}
