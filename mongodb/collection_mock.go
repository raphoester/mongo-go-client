package mongodb

import (
	"context"
	"reflect"

	"gitlab.com/raphoester/mongo-go-client/errors"
	"gitlab.com/raphoester/mongo-go-client/internal/native_utils"
	"gitlab.com/raphoester/mongo-go-client/log"
)

type collectionMock struct {
	name   string
	logger log.Ilogger
	mocks  []CollectionMock
}

type CollectionMock struct {
	Err              *errors.RestError
	ManyResults      ImanyResults
	DeleteResult     IdeleteResult
	SingleResult     IsingleResult
	InsertOneResult  IinsertOneResult
	UpdateResult     IupdateResult
	InsertManyResult IinsertManyResult
	Any              any
}

func NewCollectionMock(logger log.Ilogger, name string) *collectionMock {
	//lint:ignore S1021 _
	var ret Icollection
	ret = &collectionMock{
		name:   name,
		logger: logger,
	}
	return ret.(*collectionMock)
}

func (c *collectionMock) AddMock(mock CollectionMock) {
	c.mocks = append(c.mocks, mock)
}

func (c *collectionMock) WantFirstMock() *CollectionMock {
	if len(c.mocks) > 0 {
		var ret *CollectionMock
		c.mocks, ret = native_utils.GetAndPop(c.mocks)
		return ret
	}
	c.logger.Error("no mocks available")
	return nil
}

func (c *collectionMock) Name() string {
	return c.name
}

func (c *collectionMock) GetOne(ctx context.Context, filter any, dst any) *errors.RestError {
	mock := c.WantFirstMock()

	if mock == nil {
		return nil
	}

	if mock.Any != nil {
		v := reflect.ValueOf(dst)
		if v.Kind() != reflect.Pointer || v.IsNil() {
			c.logger.Error("invalid destination interface provided")
			return errors.NewInternalServerError()
		}

		v.Elem().Set(reflect.ValueOf(mock.Any).Elem())
	}

	return mock.Err
}

func (c *collectionMock) Aggregate(ctx context.Context, pipeline any) (ImanyResults, *errors.RestError) {
	if len(c.mocks) < 1 {
		c.logger.Error("no mockups")
		return nil, nil
	}

	return c.mocks[0].ManyResults, c.mocks[0].Err
}

func (c *collectionMock) DeleteOne(ctx context.Context, filter any) (IdeleteResult, *errors.RestError) {
	if len(c.mocks) < 1 {
		c.logger.Error("no mockups")
		return nil, nil
	}

	return c.mocks[0].DeleteResult, c.mocks[0].Err
}

func (c *collectionMock) Find(ctx context.Context, filter any, options ...FindOptions) (ImanyResults, *errors.RestError) {
	if len(c.mocks) < 1 {
		c.logger.Error("no mockups")
		return nil, nil
	}

	return c.mocks[0].ManyResults, c.mocks[0].Err
}

func (c *collectionMock) FindOne(ctx context.Context, filter any) (IsingleResult, *errors.RestError) {
	if len(c.mocks) < 1 {
		c.logger.Error("no mockups")
		return nil, nil
	}

	return c.mocks[0].SingleResult, c.mocks[0].Err
}

func (c *collectionMock) InsertOne(ctx context.Context, document any) (IinsertOneResult, *errors.RestError) {
	if len(c.mocks) < 1 {
		c.logger.Error("no mockups")
		return nil, nil
	}

	return c.mocks[0].InsertOneResult, c.mocks[0].Err
}

func (c *collectionMock) UpdateMany(ctx context.Context, filter any, update any) (IupdateResult, *errors.RestError) {
	if len(c.mocks) < 1 {
		c.logger.Error("no mockups")
		return nil, nil
	}

	return c.mocks[0].UpdateResult, c.mocks[0].Err
}

func (c *collectionMock) UpdateOne(ctx context.Context, filter any, update any) (IupdateResult, *errors.RestError) {
	mock := c.WantFirstMock()
	return mock.UpdateResult, mock.Err
}

func (c *collectionMock) InsertMany(ctx context.Context, documents []any) (IinsertManyResult, *errors.RestError) {
	mock := c.WantFirstMock()
	return mock.InsertManyResult, mock.Err
}

func (c *collectionMock) DeleteMany(context.Context, any) (IdeleteResult, *errors.RestError) {
	mock := c.WantFirstMock()
	return mock.DeleteResult, mock.Err
}
