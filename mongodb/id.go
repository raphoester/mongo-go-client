package mongodb

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Iid interface {
	Hex()
	IsZero()
}

type Id primitive.ObjectID

var NilId = primitive.NilObjectID

var SampleId = primitive.NewObjectID()
var SampleId1 = primitive.NewObjectID()
var SampleId2 = primitive.NewObjectID()
var SampleId3 = primitive.NewObjectID()
var SampleId4 = primitive.NewObjectID()
var SampleId5 = primitive.NewObjectID()
var SampleId6 = primitive.NewObjectID()
var SampleId7 = primitive.NewObjectID()
var SampleId8 = primitive.NewObjectID()
var SampleId9 = primitive.NewObjectID()

var SampleIds = []primitive.ObjectID{
	SampleId,
	SampleId1,
	SampleId2,
	SampleId4,
	SampleId5,
	SampleId6,
	SampleId7,
	SampleId8,
	SampleId9,
}

// var TestId = primitive.ObjectID{}

// func (id Id) IsZero() bool {
// 	return id == NilId
// }

func IdSlice(strings []string) []Id {
	ret := []Id{}
	for _, s := range strings {
		res, err := primitive.ObjectIDFromHex(s)
		if err == nil {
			ret = append(ret, Id(res))
		}
	}

	return ret
}

// // Retour sans pointeur de la variable pour autoriser l'ignorance d'erreurs
// func IdFromHex(str string) (Id, error) {
// 	ret, err := primitive.ObjectIDFromHex(str)
// 	if err != nil {
// 		return NilId, fmt.Errorf("invalid id hex value")
// 	}
// 	return Id(ret), nil
// }

// func (id Id) Hex() string {
// 	return ""
// }
