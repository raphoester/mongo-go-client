package mongodb

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/raphoester/mongo-go-client/errors"
	"gitlab.com/raphoester/mongo-go-client/log"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Imongodb interface {
	Connect() error
	ConnectToCollection(string) Icollection
	Transaction(func(Context) (any, error)) (any, *errors.RestError)
}

type mongodb struct {
	Creds  Credentials
	DB     *mongo.Database
	logger log.Ilogger
}

type Credentials struct {
	DbName string
	Url    string
}

func NewMongo(logger log.Ilogger, creds Credentials) *mongodb {
	//lint:ignore S1021 _
	var ret Imongodb
	ret = &mongodb{
		Creds:  creds,
		logger: logger,
	}

	return ret.(*mongodb)
}

func (m *mongodb) Connect() error {
	client, err := mongo.NewClient(options.Client().ApplyURI(m.Creds.Url))
	if err != nil {
		return fmt.Errorf("failed creating mongo client | %s", err.Error())
	}
	m.logger.Core("successfully created mongo client")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := client.Connect(ctx); err != nil {
		return fmt.Errorf("failed connecting to mongo | %s", err.Error())
	}

	m.logger.Core("successfully connected mongo client to mongodb")

	db := client.Database(m.Creds.DbName)
	m.DB = db

	return nil
}

func (m mongodb) ConnectToCollection(collectionName string) Icollection {
	native := m.DB.Collection(collectionName)
	return collection{
		c:      native,
		logger: m.logger,
	}
}
