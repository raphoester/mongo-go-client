package mongodb

import (
	"context"
	"net/http"
	"reflect"

	"gitlab.com/raphoester/mongo-go-client/errors"
	"gitlab.com/raphoester/mongo-go-client/internal/native_utils"
	"gitlab.com/raphoester/mongo-go-client/log"
	"go.mongodb.org/mongo-driver/mongo"
)

type IsingleResult interface {
	Decode(dest any) *errors.RestError
}

type singleResult struct {
	logger  log.Ilogger
	content *mongo.SingleResult
}

func newSingleResult(content *mongo.SingleResult, logger log.Ilogger) *singleResult {
	//lint:ignore S1021 _
	var ret IsingleResult
	ret = &singleResult{
		logger:  logger,
		content: content,
	}
	return ret.(*singleResult)
}

func (r *singleResult) Decode(dest any) *errors.RestError {
	rv := reflect.ValueOf(dest)
	if rv.Kind() != reflect.Pointer {
		r.logger.Error("destination struct is not a pointer")
		return errors.NewInternalServerError()
	}

	if err := r.content.Decode(dest); err != nil {
		r.logger.Error("failed parsing result to struct | %s", err.Error())
		return &errors.RestError{
			Status: http.StatusInternalServerError,
		}
	}

	r.logger.Debug("successfully parsed result to destination struct")
	return nil
}

type ImanyResults interface {
	All(dest any) *errors.RestError
}

type manyResults struct {
	content *mongo.Cursor
	logger  log.Ilogger
}

func newManyResults(content *mongo.Cursor, logger log.Ilogger) *manyResults {
	return &manyResults{
		content: content,
		logger:  logger,
	}
}

func (r *manyResults) All(dest any) *errors.RestError {
	if err := r.content.All(context.TODO(), dest); err != nil {
		r.logger.Error("failed parsing results to struct | %s", err.Error())
		return &errors.RestError{
			Status: http.StatusInternalServerError,
		}
	}

	// make sure the dest is not nil
	native_utils.InitializeSlice(dest)

	r.logger.Debug("successfully parsed results to destination struct")
	return nil
}

type IupdateResult interface {
	MatchedCount() int64
	ModifiedCount() int64
}

type updateResult struct {
	content *mongo.UpdateResult
}

func newUpdateResult(content *mongo.UpdateResult) *updateResult {
	return &updateResult{
		content: content,
	}
}

func (r *updateResult) MatchedCount() int64 {
	return r.content.MatchedCount
}

func (r *updateResult) ModifiedCount() int64 {
	return r.content.ModifiedCount
}

type IdeleteResult interface {
	DeletedCount() int64
}

type deleteResult struct {
	content *mongo.DeleteResult
}

func newDeleteResult(content *mongo.DeleteResult) *deleteResult {
	return &deleteResult{
		content: content,
	}
}

func (r *deleteResult) DeletedCount() int64 {
	return r.content.DeletedCount
}

type IinsertOneResult interface {
	InsertedId() any
}

type insertOneResult struct {
	content *mongo.InsertOneResult
}

func newInsertOneResult(content *mongo.InsertOneResult) *insertOneResult {
	return &insertOneResult{
		content: content,
	}
}

func (r *insertOneResult) InsertedId() any {
	return r.content.InsertedID
}

type IinsertManyResult interface {
	InsertedIds() []any
}

type insertManyResult struct {
	content *mongo.InsertManyResult
}

func (r *insertManyResult) InsertedIds() []any {
	return r.content.InsertedIDs
}

func NewInsertManyResult(content *mongo.InsertManyResult) *insertManyResult {
	return &insertManyResult{
		content: content,
	}
}
