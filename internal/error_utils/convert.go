package error_utils

import (
	"net/http"
	"reflect"

	"gitlab.com/raphoester/mongo-go-client/errors"
)

func NativeErrorToCustom(err error) *errors.RestError {
	if err == nil {
		return nil
	}

	rv := reflect.ValueOf(err)

	message := ""
	status := 0

	if rv.Kind() != reflect.Pointer {
		return errors.NewRestError(http.StatusInternalServerError, err.Error())
	}

	rv = reflect.Indirect(rv)

	// if the error is not a custom struct
	if rv.Kind() != reflect.Struct {
		return errors.NewRestError(http.StatusInternalServerError, err.Error())
	}

	messageField := rv.FieldByName("Message")
	// handle undefined value
	if messageField == (reflect.Value{}) {
		return errors.NewRestError(http.StatusInternalServerError, err.Error())
	}

	statusField := rv.FieldByName("Status")
	// handle undefined value
	if statusField == (reflect.Value{}) {
		return errors.NewRestError(http.StatusInternalServerError, err.Error())
	}

	if messageField.Kind() == reflect.String {
		message = messageField.String()
	}

	if statusField.Kind() == reflect.Int {
		status = int(statusField.Int())
	}

	return errors.NewRestError(status, message)
}
