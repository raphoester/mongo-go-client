package error_utils

import (
	"fmt"
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/raphoester/mongo-go-client/errors"
)

func TestNativeErrorToCustom(t *testing.T) {
	testCases := []struct {
		name        string
		input       error
		ExpectedRet *errors.RestError
	}{
		{
			name:        "nil error",
			input:       nil,
			ExpectedRet: nil,
		},
		{
			name:  "non-struct input error",
			input: fmt.Errorf("non-struct error"),
			ExpectedRet: &errors.RestError{
				Status:  http.StatusInternalServerError,
				Message: "non-struct error",
			},
		},
		{
			name: "valid test case",
			input: &struct {
				Status  int
				Message string
				error
			}{
				Status:  http.StatusBadRequest,
				Message: "invalid request",
				error:   fmt.Errorf(""),
			},
			ExpectedRet: &errors.RestError{
				Status:  http.StatusBadRequest,
				Message: "invalid request",
			},
		},
		{
			name: "struct error with only message field",
			input: &struct {
				Message string
				error
			}{
				Message: "missing field",
				error:   fmt.Errorf(""),
			},
			ExpectedRet: &errors.RestError{
				Status:  http.StatusInternalServerError,
				Message: "",
			},
		},
		{
			name: "struct error with only status field",
			input: &struct {
				Status int
				error
			}{
				Status: http.StatusBadRequest,
				error:  fmt.Errorf(""),
			},
			ExpectedRet: &errors.RestError{
				Status:  http.StatusInternalServerError,
				Message: "",
			},
		},
	}

	for _, tc := range testCases {
		t.Run(
			tc.name, func(t *testing.T) {
				actualRet := NativeErrorToCustom(tc.input)
				if !reflect.DeepEqual(actualRet, tc.ExpectedRet) {
					t.Errorf("expected %v but got %v", tc.ExpectedRet, actualRet)
				}
			},
		)
	}
}
