package native_utils

import (
	"fmt"
	"reflect"
)

func Pop[anything any](slice []anything) []anything {
	if len(slice) == 0 {
		return slice
	}
	return slice[1:]
}

func GetAndPop[anything any](slice []anything) ([]anything, *anything) {
	if len(slice) == 0 {
		return slice, nil
	}

	value := slice[0]
	return Pop(slice), &value
}

func InitializeSlice(slicePtr any) error {
	rv := reflect.ValueOf(slicePtr)
	if rv.Kind() != reflect.Ptr {
		return fmt.Errorf("pointer to slice expected, recieved %s", rv.Kind())
	}

	elem := rv.Elem()
	if elem.Kind() != reflect.Slice {
		return fmt.Errorf("pointer to slice expected, recieved %s", rv.Kind())
	}

	if elem.IsNil() {
		valueType := rv.Type().Elem()
		slice := reflect.MakeSlice(valueType, 0, 0)
		elem.Set(slice)
	}

	return nil
}
