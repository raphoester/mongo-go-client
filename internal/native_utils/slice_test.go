package native_utils_test

import (
	"reflect"
	"testing"

	"gitlab.com/raphoester/mongo-go-client/internal/native_utils"
)

func TestInitializeSlice(t *testing.T) {
	randomStruct := struct {
		int
		*string
	}{
		5,
		func(a string) *string {
			return &a
		}(""),
	}

	testCases := []struct {
		name     string
		input    interface{}
		expected interface{}
		wantErr  bool
	}{
		{
			name:     "valid slice ptr",
			input:    new([]int),
			expected: &[]int{},
			wantErr:  false,
		}, {
			name:     "pointer filled with random shit",
			input:    &randomStruct,
			expected: &randomStruct,
			wantErr:  true,
		}, {
			name:    "not a pointer",
			input:   []int{},
			wantErr: true,
		},
	}

	for _, tc := range testCases {
		t.Run(
			tc.name,
			func(t *testing.T) {
				err := native_utils.InitializeSlice(tc.input)
				if (err != nil) != tc.wantErr {
					t.Errorf("InitializeSlice(%v) = %v, wantErr %v", tc.input, err, tc.wantErr)
				}

				if !tc.wantErr {
					if !reflect.DeepEqual(tc.input, tc.expected) {
						t.Errorf("InitializeSlice(%v) = %v, expected %v", tc.input, tc.input, tc.expected)
					}
				}
			},
		)
	}
}
